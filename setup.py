from setuptools import setup, find_packages

requires = [
    'tornado',
    'tornado-sqlalchemy',
    'psycopg2',
    'postgresql',
    'pip',
    'pytradfri',
    'aiocoap',
    'DTLSSocket',
    'firebase-admin'
]

setup(
    name='smartyhabackend',
    version='0.0',
    description='A SmartHome backend server built with Tornado',
    author='Zvonimir Sabados',
    author_email='zwonne.rk@gmail.com',
    keywords='web tornado',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    entry_points={
        'console_scripts': [
            'serve_app = smartyhabackend:main',
            'initdb = initializedb:main'
        ],
    },
)
