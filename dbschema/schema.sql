--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tradfri_gateways; Type: TABLE; Schema: public; Owner: zvonimir
--

CREATE TABLE public.tradfri_gateways (
    psk text NOT NULL,
    identity text NOT NULL,
    host text NOT NULL,
    label text NOT NULL,
    icon text,
    owner_uid text NOT NULL
);


ALTER TABLE public.tradfri_gateways OWNER TO zvonimir;

--
-- Name: users; Type: TABLE; Schema: public; Owner: zvonimir
--

CREATE TABLE public.users (
    uid text NOT NULL,
    email text
);


ALTER TABLE public.users OWNER TO zvonimir;

--
-- Name: users primary_key; Type: CONSTRAINT; Schema: public; Owner: zvonimir
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT primary_key PRIMARY KEY (uid);


--
-- Name: tradfri_gateways psk_primary_key; Type: CONSTRAINT; Schema: public; Owner: zvonimir
--

ALTER TABLE ONLY public.tradfri_gateways
    ADD CONSTRAINT psk_primary_key PRIMARY KEY (psk);


--
-- Name: users uniq; Type: CONSTRAINT; Schema: public; Owner: zvonimir
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT uniq UNIQUE (email);


--
-- Name: tradfri_gateways user_foreign_key; Type: FK CONSTRAINT; Schema: public; Owner: zvonimir
--

ALTER TABLE ONLY public.tradfri_gateways
    ADD CONSTRAINT user_foreign_key FOREIGN KEY (owner_uid) REFERENCES public.users(uid);


--
-- PostgreSQL database dump complete
--

