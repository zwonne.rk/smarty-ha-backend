import json
import pprint
from typing import NamedTuple

from firebase_admin import auth
from tornado_sqlalchemy import SessionMixin
from tornado.web import RequestHandler


class BaseAPI(RequestHandler, SessionMixin):
    """Base API class for this application."""

    def set_default_headers(self):
        """Set the default response header to be JSON."""
        self.set_header("Content-Type", 'application/json; charset="utf-8"')

    def send_response(self, data, status=200):
        """Construct and send a JSON response with appropriate status code."""
        self.set_status(status)
        self.write(json.dumps(data))

    async def verify_id_token(self, token_id):
        return auth.verify_id_token(token_id)


class ServerPingAPI(BaseAPI):
    """API for checking connection to the server"""
    SUPPORTED_METHODS = ["GET"]

    def get(self):
        pprint.pprint("ServerPingAPI: get()")
        self.send_response('true')


class TradfriLightBulb(NamedTuple):
        """Class for mapping pyTradfri's device objects into dicts"""
        gateway_psk: str
        bulb_id: int
        user_id: str
        name: str
        last_seen: int  # unix timestamp
        reachable: bool
        is_on: int  # 0 or 1
        dimmer_value: int  # 1 to 254
        manufacturer: str
        model_number: str
        firmware_version: str

class TradfriAPI(BaseAPI):
    """Base API for working with Tradfri"""
    SUPPORTED_METHODS = ["GET"]

    @staticmethod
    def light_to_dict(light, gateway_psk, user_id):
        return TradfriLightBulb(gateway_psk, light.id, user_id, light.name, light.raw['9020'], light.reachable,
                                light.raw['3311'][0]['5850'], light.raw['3311'][0]['5851'],
                                light.device_info.manufacturer, light.device_info.model_number,
                                light.device_info.firmware_version)._asdict()

    @staticmethod
    def lights_to_dicts(self, lights_list, gateway_psk, user_id):
        n = 0
        while n < len(lights_list):
            light = lights_list[n]
            lights_list[n] = self.light_to_dict(light, gateway_psk, user_id)
            n += 1
        return lights_list
