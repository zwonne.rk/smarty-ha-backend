import pprint
from asyncio import CancelledError

import tornado.escape
import tornado.platform.asyncio
from firebase_admin.auth import AuthError
from pytradfri import Gateway
from pytradfri.api.aiocoap_api import APIFactory
from tornado_sqlalchemy import as_future

from smartyhabackend.api_calls.general import BaseAPI
from smartyhabackend.api_calls.general import TradfriAPI
from smartyhabackend.model.db_entities import entity_as_dict
from smartyhabackend.model.db_entities import TradfriGateway


class TradfriGatewayAPI(BaseAPI):
    """API for reading and adding new tradfri gateways"""
    SUPPORTED_METHODS = ["GET", "POST", "PUT"]

    async def get(self, email):
        """Get Tradfri gateways for a user."""
        pprint.pprint("TradfriGatewayAPI: get()")
        with self.make_session() as session:
            try:
                id_token_from_header = self.request.headers.get('X-UserToken')
                decoded_id_token = await self.verify_id_token(id_token_from_header)
            except ValueError as valueError:
                pprint.pprint(valueError)
                raise
            except AuthError as authError:
                pprint.pprint(authError)
                raise

            if decoded_id_token:
                uid_from_token = decoded_id_token['uid']
                email_from_token = decoded_id_token['email']

                if email_from_token == email:
                    pprint.pprint("TradfriGatewayAPI: get() auth successful")
                    gateways = await as_future(
                        session.query(TradfriGateway).filter_by(owner_uid=uid_from_token).all)
                    if gateways:
                        print("TradfriGatewayAPI: get() User has registered gateways.")
                        result = list()
                        for gateway in gateways:
                            result.append(entity_as_dict(gateway))

                        self.send_response(result)
                    else:
                        pprint.pprint("TradfriGatewayAPI: get() There are no registered gateways for this user")
                        self.send_response(list())
                else:
                    pprint.pprint("TradfriGatewayAPI: get() Token data inconsistent with provided path param")
                    self.send_error(status_code=403, reason="Token data inconsistent with provided path param")
            else:
                pprint.pprint("TradfriGatewayAPI: get() Invalid token")
                self.send_error(status=401, reason="Invalid token")

    async def post(self, unknown_arg):
        """Create a new Tradfri gateway."""
        with self.make_session() as session:
            pprint.pprint("TradfriGatewayAPI: post()")
            try:
                id_token_from_header = self.request.headers.get('X-UserToken')
                decoded_id_token = await self.verify_id_token(id_token_from_header)
            except ValueError as valueError:
                pprint.pprint(valueError)
                raise
            except AuthError as authError:
                pprint.pprint(authError)
                raise

            if decoded_id_token:
                pprint.pprint("TradfriGatewayAPI: post() auth successful")

                uid_from_token = decoded_id_token['uid']
                uid_from_request_body = tornado.escape.json_decode(self.request.body).get("owner_uid", None)

                if uid_from_token == uid_from_request_body:
                    host_from_request_body = tornado.escape.json_decode(self.request.body).get("host", None)
                    security_code_from_request_body = tornado.escape.json_decode(self.request.body).get("security_code",
                                                                                                        None)
                    label_from_request_body = tornado.escape.json_decode(self.request.body).get("label", None)
                    icon_from_request_body = tornado.escape.json_decode(self.request.body).get("icon", None)
                    identity_from_request_body = tornado.escape.json_decode(self.request.body).get("identity", None)
                    print('Identity from request: ', identity_from_request_body)

                    identity = identity_from_request_body.encode('utf-8').hex()
                    print('Generated Identity: ', identity)

                    try:
                        api_factory = APIFactory(host=host_from_request_body, psk_id=identity)
                        new_psk = await tornado.platform.asyncio.to_asyncio_future(
                            api_factory.generate_psk(security_code_from_request_body))
                        print('Generated PSK: ', new_psk)

                        gateway = TradfriGateway(
                            psk=new_psk,
                            identity=identity,
                            host=host_from_request_body,
                            label=label_from_request_body,
                            icon=icon_from_request_body,
                            owner_uid=uid_from_token
                        )

                        session.add(gateway)
                        pprint.pprint("TradfriGatewayAPI: post() New gateway is successfully registered!")
                        self.send_response(new_psk, status=201)
                    except TypeError:
                        pprint.pprint("TradfriGatewayAPI: post() The pre-shared-key already exist")
                        self.send_error(status_code=409, reason="Already exists")
                else:
                    pprint.pprint("TradfriGatewayAPI: post() Token data inconsistent with request body data")
                    self.send_error(status_code=403, reason="Token data inconsistent with request body data")
            else:
                pprint.pprint("TradfriGatewayAPI: post() Invalid token")
                self.send_error(status_code=401, reason="Invalid token")

    async def put(self):
        """Update an existing Tradfri gateway."""
        with self.make_session() as session:
            pprint.pprint("TradfriGatewayAPI: put()")
            try:
                id_token_from_header = self.request.headers.get('X-UserToken')
                decoded_id_token = await self.verify_id_token(id_token_from_header)
            except ValueError as valueError:
                pprint.pprint(valueError)
                raise
            except AuthError as authError:
                pprint.pprint(authError)
                raise

            if decoded_id_token:
                pprint.pprint("TradfriGatewayAPI: put() auth successful")

                uid_from_token = decoded_id_token['uid']
                uid_from_request_body = tornado.escape.json_decode(self.request.body).get("owner_uid", None)

                if uid_from_token == uid_from_request_body:
                    psk_from_request_body = tornado.escape.json_decode(self.request.body).get("psk", None)
                    host_from_request_body = tornado.escape.json_decode(self.request.body).get("host", None)
                    label_from_request_body = tornado.escape.json_decode(self.request.body).get("label", None)
                    icon_from_request_body = tornado.escape.json_decode(self.request.body).get("icon", None)

                    try:
                        r_updated = session.query(TradfriGateway). \
                            filter(TradfriGateway.psk == psk_from_request_body,
                                   TradfriGateway.owner_uid == uid_from_request_body). \
                            update({"host": host_from_request_body,
                                    "label": label_from_request_body,
                                    "icon": icon_from_request_body})

                        pprint.pprint("TradfriGatewayAPI: put() Number of Gateways updated: " + str(r_updated))
                        self.send_response(psk_from_request_body, status=200)
                    except Exception as e:
                        print("{}".format(type(e).__name__))
                        print("TradfriGatewayAPI: put() Error occurred while updating: {}".format(e))
                        self.send_error(status_code=400, reason="Update failed")
                else:
                    pprint.pprint("TradfriGatewayAPI: put() Token data inconsistent with request body data")
                    self.send_error(status_code=403, reason="Token data inconsistent with request body data")
            else:
                pprint.pprint("TradfriGatewayAPI: put() Invalid token")
                self.send_error(status_code=401, reason="Invalid token")


class TradfriLightBulbAPI(TradfriAPI):
    """API for reading and adding new tradfri gateways"""
    SUPPORTED_METHODS = ["GET"]

    async def get(self, email):
        """Get Tradfri LightBulbs for a user."""
        pprint.pprint("TradfriLightBulbAPI: get(email)")
        with self.make_session() as session:
            try:
                id_token_from_header = self.request.headers.get('X-UserToken')
                decoded_id_token = await self.verify_id_token(id_token_from_header)
            except ValueError as valueError:
                pprint.pprint(valueError)
                raise
            except AuthError as authError:
                pprint.pprint(authError)
                raise
            except CancelledError as cancelledError:
                pprint.pprint(cancelledError)
                raise

            if decoded_id_token:
                uid_from_token = decoded_id_token['uid']
                email_from_token = decoded_id_token['email']

                if email_from_token == email:
                    pprint.pprint("TradfriLightBulbAPI: get(email) auth successful")
                    gateways = await as_future(
                        session.query(TradfriGateway).filter_by(owner_uid=uid_from_token).all)
                    if gateways:
                        print("TradfriLightBulbAPI: get(email) User has registered gateways.")
                        result = list()
                        for gateway in gateways:
                            api_factory = APIFactory(gateway.host, gateway.identity, gateway.psk)
                            api = api_factory.request
                            gteway = Gateway()
                            devices_command = gteway.get_devices()
                            devices_commands = await api(devices_command)
                            devices = await api(devices_commands)
                            lights = [dev for dev in devices if dev.has_light_control]
                            dicts = self.lights_to_dicts(self, lights, gateway.psk, uid_from_token)
                            result.extend(dicts)

                        pprint.pprint(
                            "TradfriLightBulbAPI: get(email) Number of LightBulbs returned: " + str(len(result)))
                        self.send_response(result, status=200)
                    else:
                        pprint.pprint("TradfriLightBulbAPI: get(email) There are no registered gateways for this user")
                        self.send_response(list(), status=200)
                else:
                    pprint.pprint("TradfriLightBulbAPI: get(email) Token data inconsistent with provided path param")
                    self.send_error(status_code=403, reason="Token data inconsistent with provided path param")
            else:
                pprint.pprint("TradfriLightBulbAPI: get(email) Invalid token")
                self.send_error(status=401, reason="Invalid token")
