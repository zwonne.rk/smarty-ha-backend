import pprint

import tornado.escape
from tornado_sqlalchemy import as_future
from firebase_admin.auth import AuthError

from smartyhabackend.model.db_entities import User
from smartyhabackend.api_calls.general import BaseAPI


class UserAPI(BaseAPI):
    """API for reading and adding new users"""
    SUPPORTED_METHODS = ["GET", "POST"]

    async def get(self, email):
        """Check if a user exists."""
        pprint.pprint("UserAPI: get()")
        with self.make_session() as session:
            try:
                id_token_from_header = self.request.headers.get('X-UserToken')
                decoded_id_token = await self.verify_id_token(id_token_from_header)
            except ValueError as valueError:
                pprint.pprint(valueError)
                raise
            except AuthError as authError:
                pprint.pprint(authError)
                raise

            if decoded_id_token:
                email_from_token = decoded_id_token['email']

                if email_from_token == email:
                    pprint.pprint("UserAPI: get() auth successful")
                    user = await as_future(session.query(User).filter(User.email == email).first)
                    if user:
                        pprint.pprint("UserAPI: get() The user is already registered!")
                        self.send_response('true')
                    else:
                        pprint.pprint("UserAPI: get() The user is not registered!")
                        self.send_response('false')
                else:
                    pprint.pprint("UserAPI: get() Token data inconsistent with provided path param")
                    self.send_error(status_code=403, reason="Token data inconsistent with provided path param")
            else:
                pprint.pprint("UserAPI: get() Invalid token")
                self.send_error(status=401, reason="Invalid token")

    async def post(self):
        """Create a new user."""
        with self.make_session() as session:
            pprint.pprint("UserAPI: post()")
            try:
                id_token_from_header = self.request.headers.get('X-UserToken')
                decoded_id_token = await self.verify_id_token(id_token_from_header)
            except ValueError as valueError:
                pprint.pprint(valueError)
                raise
            except AuthError as authError:
                pprint.pprint(authError)
                raise

            if decoded_id_token:
                uid_from_token = decoded_id_token['uid']
                email_from_token = decoded_id_token['email']
                email_from_request_body = tornado.escape.json_decode(self.request.body).get("email", None)

                if email_from_token == email_from_request_body:
                    user = await as_future(session.query(User).filter(User.email == email_from_request_body).first)

                    if user:
                        pprint.pprint("UserAPI: post() The user was already registered!")
                        self.send_error(status=409, reason="The user was already registered")
                    else:
                        user = User(uid=uid_from_token, email=email_from_request_body)
                        session.add(user)
                        pprint.pprint("UserAPI: post() The user is successfully registered!")
                        self.send_response('true', status=201)
                else:
                    pprint.pprint("UserAPI: post() Token data inconsistent with request body data")
                    self.send_error(status_code=403, reason="Token data inconsistent with request body data")
            else:
                pprint.pprint("UserAPI: post() Invalid token")
                self.send_error(status_code=401, reason="Invalid token")
