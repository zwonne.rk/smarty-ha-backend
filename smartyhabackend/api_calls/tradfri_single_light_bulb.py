import pprint

import tornado
from firebase_admin.auth import AuthError
from pytradfri import Gateway
from pytradfri.api.aiocoap_api import APIFactory
from tornado_sqlalchemy import as_future

from smartyhabackend.api_calls.general import TradfriAPI
from smartyhabackend.model.db_entities import TradfriGateway


class TradfriSingleLightBulbAPI(TradfriAPI):
    """API for reading and updating a single tradfri lightbulb"""
    SUPPORTED_METHODS = ["GET", "PUT"]

    async def get(self, email, psk, bulb_id):
        """Get Tradfri LightBulb for a psk and bulb_id."""
        pprint.pprint("TradfriLightBulbAPI: get(email, psk, bulb_id)")
        with self.make_session() as session:
            try:
                id_token_from_header = self.request.headers.get('X-UserToken')
                decoded_id_token = await self.verify_id_token(id_token_from_header)
            except ValueError as valueError:
                pprint.pprint(valueError)
                raise
            except AuthError as authError:
                pprint.pprint(authError)
                raise

            if decoded_id_token:
                uid_from_token = decoded_id_token['uid']
                email_from_token = decoded_id_token['email']

                if email_from_token == email:
                    result = list()
                    pprint.pprint("TradfriLightBulbAPI: get(email, psk, bulb_id) auth successful")
                    gateways = await as_future(
                        session.query(TradfriGateway).filter_by(owner_uid=uid_from_token, psk=psk).all)
                    gateway = gateways[0]
                    api_factory = APIFactory(gateway.host, gateway.identity, gateway.psk)
                    api = api_factory.request
                    gteway = Gateway()
                    light_bulb = await api(gteway.get_device(bulb_id))
                    if light_bulb:
                        pprint.pprint("TradfriLightBulbAPI: get(email, psk, bulb_id) lightBulb found")
                        dict = self.light_to_dict(light_bulb, psk, uid_from_token)
                        pprint.pprint("TradfriLightBulbAPI: get(email, psk, bulb_id) lightBulb dimmer: "
                                      + str(dict['dimmer_value']) + ", isOn: " + str(dict['is_on']))
                        result.append(dict.copy())
                        self.send_response(result, status=200)
                    else:
                        pprint.pprint("TradfriLightBulbAPI: get(email, psk, bulb_id) lightBulb not found")
                        self.send_response(result, status=200)
                else:
                    pprint.pprint(
                        "TradfriLightBulbAPI: get(email, psk, bulb_id) Token data inconsistent with provided path param")
                    self.send_error(status_code=403, reason="Token data inconsistent with provided path param")
            else:
                pprint.pprint("TradfriLightBulbAPI: get(email, psk, bulb_id) Invalid token")
                self.send_error(status=401, reason="Invalid token")

    async def put(self):
        """Update an existing Tradfri LightBulb."""
        with self.make_session() as session:
            pprint.pprint("TradfriLightBulbAPI: put()")
            try:
                id_token_from_header = self.request.headers.get('X-UserToken')
                decoded_id_token = await self.verify_id_token(id_token_from_header)
            except ValueError as valueError:
                pprint.pprint(valueError)
                raise
            except AuthError as authError:
                pprint.pprint(authError)
                raise

            if decoded_id_token:
                pprint.pprint("TradfriLightBulbAPI: put() auth successful")

                uid_from_token = decoded_id_token['uid']
                uid_from_request_body = tornado.escape.json_decode(self.request.body).get("owner_uid", None)

                if uid_from_token == uid_from_request_body:
                    psk_from_request_body = tornado.escape.json_decode(self.request.body).get("gatewayPsk", None)
                    bulb_id_from_request_body = tornado.escape.json_decode(self.request.body).get("bulbId", None)
                    is_on_from_request_body = tornado.escape.json_decode(self.request.body).get("isOn", None)
                    dimmer_value_from_request_body = tornado.escape.json_decode(self.request.body).get("dimmerValue",
                                                                                                       None)

                    gateways = await as_future(
                        session.query(TradfriGateway).filter_by(owner_uid=uid_from_token,
                                                                psk=psk_from_request_body).all)
                    gateway = gateways[0]
                    api_factory = APIFactory(gateway.host, gateway.identity, gateway.psk)
                    api = api_factory.request
                    gteway = Gateway()
                    light_bulb = await api(gteway.get_device(bulb_id_from_request_body))
                    if light_bulb:
                        pprint.pprint("TradfriLightBulbAPI: put() turn on: " + str(is_on_from_request_body))
                        pprint.pprint("TradfriLightBulbAPI: put() dimmer value: " + str(dimmer_value_from_request_body))
                        await api(light_bulb.light_control.set_dimmer(dimmer_value_from_request_body))
                        await api(light_bulb.light_control.set_state(is_on_from_request_body))
                        # return LastSeen, and Reachable in response
                        self.send_response({"first": light_bulb.raw['9020'], "second": light_bulb.reachable},
                                           status=200)
                    else:
                        pprint.pprint("TradfriLightBulbAPI: put() lightBulb not found")
                        self.send_error(status_code=400, reason="The Tradfri LightBulb is not found")
                else:
                    pprint.pprint("TradfriLightBulbAPI: put() Token data inconsistent with request body data")
                    self.send_error(status_code=403, reason="Token data inconsistent with request body data")
            else:
                pprint.pprint("TradfriLightBulbAPI: put() Invalid token")
                self.send_error(status_code=401, reason="Invalid token")
