import os

import asyncio
import firebase_admin
from firebase_admin import credentials
from tornado.httpserver import HTTPServer
from tornado.options import options
from tornado.platform.asyncio import AsyncIOMainLoop
from tornado_sqlalchemy import make_session_factory
from tornado.web import Application

import smartyhabackend.my_options
from smartyhabackend.api_calls.general import ServerPingAPI
from smartyhabackend.api_calls.user import UserAPI
from smartyhabackend.api_calls.tradfri_single_light_bulb import TradfriSingleLightBulbAPI
from smartyhabackend.api_calls.tradfri import (
    TradfriGatewayAPI,
    TradfriLightBulbAPI,
)

factory = make_session_factory(os.environ.get('DATABASE_URL', ''))


def main():
    """Construct and initialize firebase app."""
    cred = credentials.Certificate('../certs/smartyha-3d7a9-firebase-adminsdk-7pmx4-8c45390c72.json')
    firebase_app = firebase_admin.initialize_app(cred)

    """Construct and serve the tornado application."""
    app = Application([
        (r'/api/v1/ping/', ServerPingAPI),  # get
        (r'/api/v1/accounts/', UserAPI),  # post
        (r'/api/v1/accounts/([^/]*)', UserAPI),  # get
        (r'/api/v1/devices/tradfri/gateways/', TradfriGatewayAPI),  # post, put
        (r'/api/v1/devices/tradfri/gateways/([^/]*)', TradfriGatewayAPI),  # get
        (r'/api/v1/devices/tradfri/bulbs/([^/]*)', TradfriLightBulbAPI),  # get
        (r'/api/v1/devices/tradfri/bulb/([^/]*)/([^/]*)/([^/]*)', TradfriSingleLightBulbAPI),  # get
        (r'/api/v1/devices/tradfri/bulb/', TradfriSingleLightBulbAPI)  # put
    ],
        session_factory=factory
    )
    AsyncIOMainLoop().install()

    http_server = HTTPServer(app,
                             ssl_options={
                                 'certfile': os.path.join("../certs/smartyha.crt"),
                                 'keyfile': os.path.join("../certs/smartyha.key")
                             })
    http_server.listen(options.port)
    print('Listening on http://localhost:%i' % options.port)
    asyncio.get_event_loop().run_forever()


if __name__ == "__main__":
    main()
