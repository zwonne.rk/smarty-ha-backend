from typing import Dict

from sqlalchemy import (
    Column,
    String,
    ForeignKey
)
from tornado_sqlalchemy import declarative_base
from sqlalchemy import inspect

DeclarativeBase = declarative_base()


def entity_as_dict(entity) -> Dict[str, str]:
    return {c.key: getattr(entity, c.key)
            for c in inspect(entity).mapper.column_attrs}


class User(DeclarativeBase):
    __tablename__ = 'users'

    uid = Column(String(255), primary_key=True)
    email = Column(String(255), unique=True)


class TradfriGateway(DeclarativeBase):
    __tablename__ = 'tradfri_gateways'

    psk = Column(String(255), primary_key=True, nullable=False)
    identity = Column(String(255), nullable=False)
    host = Column(String(255), nullable=False)
    label = Column(String(255), nullable=False)
    icon = Column(String(255), nullable=True)
    owner_uid = Column(String(255), ForeignKey("users.uid"), nullable=False)
