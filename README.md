# SmartyHA Backend

This project is the backend part of a DIY Home Automation system, created specifically for my Masters thesis.
The project is written in Python, and is using the [Tornado](https://www.tornadoweb.org/en/stable/) framework.

## Libraries/frameworks:
- [Tornado](https://www.tornadoweb.org/en/stable/) - async web server,
- [PostgresSQL](https://www.postgresql.org/) - database,
- [SqlAlchemy](https://www.sqlalchemy.org/) - communication with database,
- [Firebase-admin](https://github.com/firebase/firebase-admin-python) - authenticating client,

## Note:
- I'm using this project using a conda environment, so having [Anaconda](https://www.anaconda.com/) installed is a plus.

## Note2:
- If you are planning to use self signed certificates for **use of https**, you need to generate your own, and put them in `<PATH_TO_CLONED_REPO_ROOT>/certs` after [Installation steps 2)](#2-clone-the-repository-and-activate-the-conda-environment) or [3)](#3-init-postgres-database-tables). This way, you would have something like: 
  - `/home/zvonimir/PythonProjects/smarty-ha-backend/certs/smartyha.crt`, and 
  - `/home/zvonimir/PythonProjects/smarty-ha-backend/certs/smartyha.key`.
- You can check instructions how to generate them on [this link](https://gist.github.com/justinhartman/36cccc6ce26a01378369b35fd048748a), or you can use the following snippet as a template, where `<YOUR_IP>`, `<YOUR_ALTERNATIVE_IP>`, and `<YOUR_ALTERNATIVE_IP2>` can be replaced with `localhost`, or `192.168.1.4.`, etc...:
```sh
$ openssl req -x509 -out smartyha.crt -keyout localhost.key \
  -newkey rsa:4096 -nodes -sha256 \
  -subj '/CN=<YOUR_IP>' -extensions EXT -config <( \
  printf "[dn]\nCN=<YOUR_IP>\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:<YOUR_ALTERNATIVE_IP>,DNS:<YOUR_ALTERNATIVE_IP2>\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```

## Installation steps
### 1) Install Postgresql
- The installation steps are borrowed from [digitalocean's site](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04). Check the link out for a more detailed postgres installation tutorial. Otherwise, you can install postgresql by typing:
```sh
$ sudo apt-get install postgresql postgresql-contrib
```
- At this point, you  just have the **postgres** role configured with the database. Create a new role (for this tutorial, I'm creating a new user called **zvonimir**. You can name your user anyway you like):
```sh
$ sudo -u postgres createuser --interactive
```
- The **--interactive** flag will prompt you for the name of the new role, and ask whether it should have superuser permissions:
```
Enter name of role to add: zvonimir
Shall the new role be a superuser? (y/n) y
```
- Your installation of Postgres now has a new user, but you don't have any databases added. Postgres authentication system by default makes an assumption that for any role used to log in, that role will have a database with the same name which it can access. This means that, if the new user you created is called **zvonimir**, that role will attempt to connect to a database which is also called "zvonimir" by default. You can create the database with the following command:
```sh
$ sudo -u postgres createdb zvonimir
```
- You can now connect to the database with the following code:
```sh
$ sudo -u zvonimir psql
```
- Once logged in, you can check you current connection information by typing:
```sh
zvonimir=# \conninfo
```
- And you can change the password for the user by typing:
```sh
zvonimir=# \password
```
- Add the tables to the database in [Installation Step 3)](#3-init-postgres-database-tables), using the **schema.sql** file from the repository.

### 2) Clone the repository, and activate the conda environment
- After cloning the repository, activate the conda environment from a .yml file. This environment will have all the necessary libraries for the project to work. 
- The following line will create the conda environment in the project's **/venv** folder with the name **'smarty-ha-backend'**
```sh
$ conda env create --file <PATH_TO_CLONED_REPO_ROOT>/conda/environment.yml --prefix <PATH_TO_CLONED_REPO_ROOT>/venv/smarty-ha-backend
```

- You can check whether the environment was successfully created with:
```sh
$ conda env list
```
- After that, add some variables to the environment - the following steps are based on [Conda's official docs](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#macos-and-linux)
- Add a new file **'<PATH_TO_CLONED_REPO_ROOT>/venv/smarty-ha-backend/etc/conda/activate.d/env_vars_sh'** to the project which will hold and set the environment variables for the 
  - Database url,
  - Server API Key,
  - etc.
  
- example of the file contents:
```sh
export DATABASE_URL='postgres://zvonimir:postgres@127.0.0.1:5432/zvonimir'
export DEBUG='True'
export SERVER_API_KEY='ASdsadasdASD-asdASDASda-aSDASDasdg-ASFg'
```

- And, add the file **'<PATH_TO_CLONED_REPO_ROOT>/venv/smarty-ha-backend/etc/conda/deactivate.d/env_vars_sh'** to the project which will unset the environment variables for the 
  - Database url,
  - Server API Key,
  - etc.
  
- example of the file contents:
```sh
unset DATABASE_URL
unset DEBUG
unset SERVER_API_KEY
```

## Note3:
- If you plan on using self signed certificates, then follow the instructions in [Note2](#note2), otherwise, you need to open `<PATH_TO_CLONED_REPO_ROOT>/smartyhabackend/__init__.py` and change the following code from
```python
    http_server = HTTPServer(app,
                             ssl_options={
                                 'certfile': os.path.join("../certs/tst.crt"),
                                 'keyfile': os.path.join("../certs/tst.key")
                             })
```
to
```python
    http_server = HTTPServer(app)
```

### 3) Init postgres database tables
- Open `<PATH_TO_CLONED_REPO_ROOT>/dbschema/schema.sql` file, and change the owner name from **zvonimir** to your role name.
- You can then add the DB tables using the `psql <DATABASE_NAME> < <PATH_TO_CLONED_REPO_ROOT>/dbschema/schema.sql` command in the terminal, i.e.:
```sh
$ psql zvonimir < /home/zvonimir/PythonProjects/smarty-ha-backend/dbschema/schema.sql
```

### 4) Install SmartyHa backend project
#### Using the command line: 
- Run `pip install -e <PATH_TO_CLONED_REPO_ROOT>/smartyhabackend` command in the terminal, i.e.:
```sh
$ pip install -e /home/zvonimir/PythonProjects/smarty-ha-backend/smartyhabackend
```
- After installing the project, you can run it through the terminal by typing `serve_app`. That will start the tornado server on your local host on port 8888.

#### Using an IDE: 
If you prefer using an IDE, i.e. [PyCharm](https://www.jetbrains.com/pycharm/), create a new run configuration, and set the script path to `<PATH_TO_CLONED_REPO_ROOT>/smartyhabackend/__init__.py`. After that, you can start the server by clicking the "run" icon in the IDE.