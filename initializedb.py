"""For initializing a new database."""
import os
from sqlalchemy import create_engine
from smartyhabackend.model.db_entities import DeclarativeBase


def main():
    """Tear down existing tables and create new ones."""
    engine = create_engine(os.environ.get('DATABASE_URL'))
    if bool(os.environ.get('DEBUG', 'True')):
        DeclarativeBase.metadata.drop_all(engine)
    DeclarativeBase.metadata.create_all(engine)
